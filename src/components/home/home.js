import React, { Component } from 'react';
import Galerie from '../galerie/galerie.js';
class Home extends Component {
  state = {
    isLoaded: false,
    home: [],
  }
  componentDidMount() {
    fetch('http://localhost/wptest/wp-json/travers/v1/frontpage')
      .then(res => res.json())
      .then((data) => {
        this.setState( {isLoaded: true, home: data } )
      })
    .catch(console.log);
  };
  render() {
    const { isLoaded, home } = this.state;
    return (
      <div>
      {isLoaded && home.galerie ? <Galerie slides={home.galerie} /> : '...'}
      <h2>Accueil</h2>
      </div>
    )
  }
}

export default Home;
