import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import './actu.scss';
import parse from 'html-react-parser';
import API from '../../api.js';

function Actu( { posts } ) {
  const [isLoaded, setLoaded] = useState( false );
  const [actu, setActu] = useState( [] );
  let { actuSlug } = useParams();
  useEffect(() => {
    let post = false;
    if( posts ) {
      post = posts.filter( actu => actu.slug === actuSlug )[0];
      setActu( post );
      setLoaded( post ? true : false );
    }
    if ( !post ) {
      API.get(`wp/v2/posts?slug=${actuSlug}`)
        .then(res => {
          setActu( res.data[0] );
          setLoaded( res.data ? true : false );
        })
        .catch(console.log);
    }
  }, []);
  return (
    <div className="actu">
      { isLoaded && actu ? <h1 className={actu}>{actu.title.rendered}</h1> : null }
      { isLoaded && actu ? parse( actu.content.rendered ) : null }
    </div>
  );
}

export default Actu;
