import React, { Component } from 'react';
import './galerieitem.css';

class GalerieItem extends Component {
  render() {
    return (
      <img src={this.props.src} alt={this.props.alt}/>
    )
  }
}

export default GalerieItem;
