import React from 'react';
import { Link } from "react-router-dom";
import './header.scss';

function Header( { loggedIn } ) {
  return (
    <div className="header">
      <ul>
        <li>
          <Link to="/">Accueil</Link>
        </li>
        <li>
          <Link to="/actualites">Actualités</Link>
        </li>
        <li>
          <Link to="/films">Films</Link>
        </li>
        <li>
          <Link to="/about">A Propos</Link>
        </li>
        <li>
          <Link to="/topics">Topics</Link>
        </li>
        <li>
          { loggedIn ? <Link to="/admin">Administration</Link> : <a href="http://localhost/wptest/wp-admin/admin.php?page=auth_app&app_name=react">Se connecter</a> }
        </li>
      </ul>

    </div>
  )
}

export default Header;
