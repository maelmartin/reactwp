import React from 'react';
import { Link, useRouteMatch } from "react-router-dom";
// import ActuItem from '../actuitem/actuitem.js';

function Actus( { posts } ) {
  let match = useRouteMatch();
  return (
    <div>
      <h1>Actualités</h1>
      {posts.map(actu => {
        return (
          <article>
            <Link to={`${match.url}/${actu.slug}`}><h2>{actu.title.rendered}</h2></Link>
          </article>
        );
      })}
    </div>
  );
}

export default Actus;
