import React, { Component } from 'react';
import parse from 'html-react-parser';
import './filmitem.css';

class Filmitem extends Component {
  render() {
    const film = this.props.film;
    return (
      <article className="actu">
        <h3>{film.title.rendered}</h3>
        <p>De {film.acf.realisateur}</p>
        <p>Avec {film.acf.acteurs}</p>
        {parse('<p>'+film.acf.synopsis+'</p>')}
      </article>
    )
  }
}

export default Filmitem;
