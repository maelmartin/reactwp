import React, { Component } from 'react';
import FilmItem from '../filmitem/filmitem.js';

class Films extends Component {
  render() {
    return (
      <div>
        <h2>Films</h2>
        {this.props.films.map(film => {
          return <FilmItem film={film}/>;
        })}
      </div>
    )
  }
}

export default Films;
