import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import GalerieItem from '../galerieitem/galerieitem.js';
import 'swiper/css/swiper.css'
import './galerie.scss';

class Galerie extends Component {
  render() {
    const params = {
      pagination: false,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      spaceBetween: 30,
      slidesPerView: 'auto',
      loop: true,
      loopedSlides: 2
    };
    return (
      <Swiper {...params}>
        {this.props.slides.map(slide => {
          return <div><GalerieItem src={slide.source} alt={slide.alt} srcset={slide.srcset}/></div>
        })}
      </Swiper>
    )
  }
}

export default Galerie;
