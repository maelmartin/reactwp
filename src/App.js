import React, { Component } from 'react';
import API from './api.js';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useRouteMatch,
  useParams
} from "react-router-dom";

import Home from './components/home/home.js';
import Header from './components/header/header.js';
import Login from './components/login/login.js';
import Admin from './components/admin/admin.js';
import Actus from './components/actus/actus.js';
import Actu from './components/actu/actu.js';
import Films from './components/films/films.js';
import About from './components/about.js';

import './App.scss';

class App extends Component {

  state = {
    posts: [],
    films: [],
    loggedIn: false,
  }
  componentDidMount() {

    API.get(`wp/v2/posts`)
      .then(res => {
        this.setState({ posts: res.data })
      })
      .catch(console.log);

    API.get(`wp/v2/films`)
      .then(res => {
        this.setState({ films: res.data })
      })
      .catch(console.log);
    };
  render() {
    return (
      <Router>
        <div className="main">
          <Header loggedIn={this.state.loggedIn} />
          <Switch>
            <Route path="/actualites/:actuSlug">
              <Actu posts={this.state.posts}/>
            </Route>
            <Route exact path="/actualites">
              <Actus posts={this.state.posts} />
            </Route>
            <Route path="/films">
              <Films films={this.state.films} />
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/topics">
              <Topics />
            </Route>
            <Route path="/admin">
              <Admin />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    )
  };
};

export default App;

function Topics() {
  let match = useRouteMatch();

  return (
    <div>
      <h2>Topics</h2>

      <ul>
        <li>
          <Link to={`${match.url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${match.url}/props-v-state`}>
            Props v. State
          </Link>
        </li>
      </ul>
      <Switch>
        <Route path={`${match.path}/:topicId`}>
          <Topic />
        </Route>
        <Route path={match.path}>
          <h3>Please select a topic.</h3>
        </Route>
      </Switch>
    </div>
  );
}

function Topic() {
  let { topicId } = useParams();
  return <h3>Requested topic ID : {topicId}</h3>;
}
